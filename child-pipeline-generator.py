import sys

def generate_job(name):
    return f"""

lint-{name}:
  extends: .lint
  variables:
    PROJECT: {name}

package-{name}:
  extends: .package
  variables:
    PROJECT: {name}
"""


def main(names):
    with open("child_pipeline.yml", "w") as f_out:
        f_out.write("include: pipeline-template.yml")
        for name in names:
            f_out.write(generate_job(name))


if __name__ == "__main__":
    names = sys.argv[1].split(",")
    main(names)


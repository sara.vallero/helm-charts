# helm-charts

Helm repository for _emfollow_ charts.

**NOTE:** only the `packages` folder and the contents of the package registry are relevant.
Everything else is just for book-keeping.

